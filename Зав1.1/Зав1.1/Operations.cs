﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Зав1._1
{
    internal class Operations
    {
        public static void CreateTextFile(string fileName, DataGridView grid)
        {
            StreamWriter sw = new StreamWriter(fileName);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                sw.WriteLine(grid[i, 0].Value);
            }
            sw.Close();
        }
        public static void ArithmeticMeanText(string fileName)
        {
            int sum = 0;
            double i = 0;

            StreamReader sr = new StreamReader(fileName);
            while(!sr.EndOfStream)
            {
                sum += Convert.ToInt32(sr.ReadLine());
                i++;
            }
            sr.Close();

            double res = sum / i;

            MessageBox.Show(res.ToString());
        }
        public static void CreateDataFile(string fileName, DataGridView grid)
        {
            FileStream fs = new FileStream(fileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);
            for (int i = 0; i < grid.ColumnCount; i++)
            {
                bw.Write(Convert.ToInt32(grid[i, 0].Value));
            }
            bw.Close();
            fs.Close();
        }
        public static void ArithmeticMeanData(string fileName)
        {
            int sum = 0;
            double i = 0;

            FileStream fs = new FileStream(fileName, FileMode.Open);
            BinaryReader br = new BinaryReader(fs);
            while(fs.Position < fs.Length)
            {
                sum += br.ReadInt32();
                i++;
            }
            br.Close();
            fs.Close();

            double res = sum / i;

            MessageBox.Show(res.ToString());
        }
    }
}
